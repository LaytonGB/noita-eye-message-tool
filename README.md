# Noita Eye Message Tool

View it here: https://laytongb.gitlab.io/noita-eye-message-tool

### Main Objectives

The intent of this project is to give a fast way to test simple ideas about the eye messages puzzle.

I indend to update this tool in my free time and the project is open to suggestions - feel free to create PRs that add functionality.

Currently I'm keeping a checklist of my own tasks in the main `+page.svelte` file, but will in future be keeping track of improvements in the gitlab issues list.
