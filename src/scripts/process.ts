type EyeGroupDirection = 'up' | 'down' | 'all';

export type Config = {
	reflect: number;
	rotate: number;
	reflectTarget: EyeGroupDirection;
	rotateTarget: EyeGroupDirection;
	outputBase: number;
	readOrderDown1: number;
	readOrderDown2: number;
	readOrderDown3: number;
	readOrderUp1: number;
	readOrderUp2: number;
	readOrderUp3: number;
};

const rotationMap = [0, 3, 4, 1, 2];

function doApply(index: number, target: EyeGroupDirection) {
	if (target === 'up') {
		return index % 2 === 1;
	} else if (target === 'down') {
		return index % 2 === 0;
	} else {
		// target === "all"
		return true;
	}
}

function dataToTriplets(data: string, config: Config): number[][][] {
	const d = data.split('5');
	let res: number[][][] = [];

	let topFirst = true;
	for (let l = 0, i = 0, j = 0; l < d.length; l += 2, i = 0, j = 0) {
		res.push([]);
		while (i < d[l].length) {
			res[res.length - 1].push([]);
			const last = res[res.length - 1];
			if (topFirst) {
				last[last.length - 1].push(parseInt(d[l][i]));
				last[last.length - 1].push(parseInt(d[l][i + 1]));
				last[last.length - 1].push(parseInt(d[l + 1][j]));
				i += 2;
				j += 1;
			} else {
				last[last.length - 1].push(parseInt(d[l + 1][j + 1]));
				last[last.length - 1].push(parseInt(d[l + 1][j]));
				last[last.length - 1].push(parseInt(d[l][i]));
				i += 1;
				j += 2;
			}
			topFirst = !topFirst;
		}
	}

	if (config.reflect === 1) {
		res = res.map((line) =>
			line.map((trip, i) => (doApply(i, config.reflectTarget) ? [trip[1], trip[0], trip[2]] : trip))
		);
	}
	// TODO not yet implemented
	// if (config.reflectCol === 2) {
	//     res = res;
	// }

	if (config.rotate === 1) {
		res = res.map((line) =>
			line.map((trip, i) =>
				doApply(i, config.rotateTarget) ? trip.map((n) => rotationMap[n]) : trip
			)
		);
	}
	//TODO not yet implemented
	// if (config.rotateCol === 2) {
	//     res = nestedMap(res, (n) => n);
	// }
	return res;
}

function tripletsToBase(triplets: number[][][], config: Config): string[][] {
	const res: string[][] = [];
	for (let l = 0; l < triplets.length; l++) {
		res.push([]);
		for (let g = 0; g < triplets[l].length; g++) {
			res[l].push(
				triplets[l][g]
					.reduce((a, b, i) => {
						return a + b * 5 ** (2 - i);
					}, 0)
					.toString(config.outputBase)
			);
		}
	}
	return res;
}

function countUnique(values: string[][]): number {
	const set = new Set();
	let res = 0;
	for (const arr of values) {
		for (const v of arr) {
			if (!set.has(v)) {
				res += 1;
				set.add(v);
			}
		}
	}
	return res;
}

function ranges(values: string[][], config: Config): string {
	const vals = [...values.flat().map((s) => parseInt(s, config.outputBase))].sort((a, b) => a - b);
	console.log(vals);
	const res: number[][] = [];
	for (const n of vals) {
		if (res.length === 0) {
			res.push([n]);
			continue;
		}
		const last = res[res.length - 1];
		if (n === last[last.length - 1] + 1) {
			if (last.length === 1) {
				last.push(n);
			} else {
				last[last.length - 1] = n;
			}
		} else if (n > last[last.length - 1]) {
			res.push([n]);
		}
	}
	return res.map((arr) => arr.join('..')).join(', ');
}

export default {
	countUnique,
	dataToTriplets,
	ranges,
	tripletsToBase
};
